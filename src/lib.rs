pub mod preview;
pub mod settings;
pub mod upload;

use actix_files::Files;
use actix_web::web::ServiceConfig;
use actix_web::{
    error::ErrorBadRequest,
    guard::Header,
    web::{post, resource, JsonConfig},
};
use serde::Serialize;
use serde_json;
use settings::CONFIG;

#[derive(Serialize)]
struct ErrResp {
    error_description: String,
}

pub fn configure_service(cfg: &mut ServiceConfig) {
    cfg.data(JsonConfig::default().error_handler(|err, _| {
        let err2 = serde_json::to_string(&ErrResp {
            error_description: err.to_string(),
        });
        ErrorBadRequest(err2.unwrap_or_else(|_| err.to_string()))
    }))
    .service(Files::new(
        &CONFIG.upload.storage.url.path(),
        &CONFIG.upload.storage.path,
    ))
    .service(
        resource("/upload")
            .data(JsonConfig::default().limit(CONFIG.upload.payload_limit))
            .route(
                post()
                    .guard(Header("content-type", mime::APPLICATION_JSON.as_ref()))
                    .to_async(upload::json),
            )
            .route(post().to_async(upload::multipart)),
    );
}
