use actix_web::HttpServer;
use actix_web::{middleware::Logger, App};
use preview_api::{configure_service, settings::CONFIG};
use std::{env, fs, io};

fn main() -> io::Result<()> {
    env::set_var("RUST_LOG", "info");
    env_logger::init();
    fs::create_dir_all(&CONFIG.upload.storage.path)?;
    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .configure(configure_service)
    })
    .bind(&CONFIG.webservice.listen)?
    .run()
}
