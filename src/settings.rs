use config::{Config, Environment, File};
use once_cell::sync::Lazy;
use serde::Deserialize;
use std::net::SocketAddr;
use std::path::PathBuf;
use url::Url;

#[derive(Debug, Deserialize)]
pub struct WebService {
    pub listen: SocketAddr,
}

#[derive(Debug, Deserialize)]
pub struct Preview {
    pub size: u32,
    pub file_suffix: String,
}

#[derive(Debug, Deserialize)]
pub struct Storage {
    pub url: Url,
    pub path: PathBuf,
}

#[derive(Debug, Deserialize)]
pub struct Upload {
    pub preview: Preview,
    pub payload_limit: usize,
    pub storage: Storage,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub upload: Upload,
    pub webservice: WebService,
}

pub static CONFIG: Lazy<Settings> = Lazy::new(|| {
    let mut config = Config::default();
    config
        .merge(File::with_name("conf/default"))
        .unwrap()
        .merge(Environment::with_prefix("PREVIEW_APP").separator("_"))
        .unwrap();
    config.try_into().unwrap()
});
