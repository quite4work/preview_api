use super::preview::make_jpg_with_preview;
use super::settings::CONFIG;
use actix_multipart::{Field, Multipart};
use actix_web::{
    error::{BlockingError, ErrorInternalServerError},
    http::header::DispositionParam,
    web::{self, Json},
    HttpResponse,
};
use awc::Client;
use base64::STANDARD;
use base64_serde::{self, base64_serde_type};
use failure::Error;
use futures::{future, future::Either, Future, Stream};
use serde::{Deserialize, Serialize};
use std::{
    collections::hash_map::DefaultHasher,
    fs,
    hash::{Hash, Hasher},
    io::{self, Write},
    path::Path,
};
use url::Url;

base64_serde_type!(Base64, STANDARD);
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum ReqImage {
    Url(Url),
    #[serde(with = "Base64")]
    Base64(Vec<u8>),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Req {
    pub images: Vec<ReqImage>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RespImage {
    pub image: Url,
    pub preview: Url,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum Resp {
    Ok { images: Vec<RespImage> },
    Err { error_description: String },
}

static INTERNAL_SERVER_ERROR: &str = "internal_server_error";

pub fn json(req: Json<Req>) -> impl Future<Item = HttpResponse, Error = actix_web::Error> {
    let client = Client::default();
    future::join_all(req.images.clone().into_iter().map(move |img| {
        match img {
            ReqImage::Url(url) => Either::A(
                client
                    .get(url.to_string())
                    .send()
                    .map_err(ErrorInternalServerError)
                    .and_then(|mut resp| resp.body().map_err(ErrorInternalServerError))
                    .and_then(|body| {
                        upload(body.as_ref().into()).map_err(ErrorInternalServerError)
                    }),
            ),
            ReqImage::Base64(data) => Either::B(upload(data).map_err(ErrorInternalServerError)),
        }
    }))
    .then(|x| match x {
        Ok(images) => HttpResponse::Ok().json(Resp::Ok { images }),
        Err(err) => {
            log::error!("{}", err);
            HttpResponse::InternalServerError().json(Resp::Err {
                error_description: INTERNAL_SERVER_ERROR.to_string(),
            })
        }
    })
}

pub fn multipart(
    multipart: Multipart,
) -> impl Future<Item = HttpResponse, Error = actix_web::Error> {
    multipart
        .filter_map(|field| {
            if !is_file(&field) {
                return None;
            };
            let res = field
                .concat2()
                .map_err(ErrorInternalServerError)
                .and_then(|bytes| upload(bytes.as_ref().into()).map_err(ErrorInternalServerError));
            Some(res.into_stream())
        })
        .flatten()
        .collect()
        .then(|x| match x {
            Ok(images) => HttpResponse::Ok().json(Resp::Ok { images }),
            Err(err) => {
                log::error!("{}", err);
                HttpResponse::InternalServerError().json(Resp::Err {
                    error_description: INTERNAL_SERVER_ERROR.to_string(),
                })
            }
        })
}

fn is_file(field: &Field) -> bool {
    if let Some(cd) = &field.content_disposition() {
        for param in &cd.parameters {
            if let DispositionParam::Filename(_) = param {
                return true;
            }
        }
    }
    false
}

fn upload(data: Vec<u8>) -> impl Future<Item = RespImage, Error = BlockingError<Error>> {
    let image_name = hex_hash(&data);
    let preview_name = [&image_name, CONFIG.upload.preview.file_suffix.as_str()].concat();
    web::block(move || {
        let (image, preview) = make_jpg_with_preview(CONFIG.upload.preview.size, &data)?;
        write_file(&image_name, &image)?;
        write_file(&preview_name, &preview)?;
        let image = to_url(&image_name)?;
        let preview = to_url(&preview_name)?;
        Ok(RespImage { image, preview })
    })
}

fn hex_hash(data: &[u8]) -> String {
    let mut hasher = DefaultHasher::new();
    data.hash(&mut hasher);
    format!("{:x}", hasher.finish())
}

fn to_url(file_name: &str) -> Result<Url, Error> {
    let file_name = [&file_name, ".jpg"].concat();
    Ok(CONFIG.upload.storage.url.join(&file_name)?)
}

fn write_file(file_name: &str, data: &[u8]) -> Result<(), io::Error> {
    let file_name = Path::new(&CONFIG.upload.storage.path)
        .join(&file_name)
        .with_extension("jpg");
    let mut file = fs::File::create(file_name)?;
    file.write_all(&data)?;
    Ok(())
}
