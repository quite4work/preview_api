use failure::{bail, ensure, Error};
use std::ffi::CStr;
use std::os::raw::c_char;
use std::slice;

#[repr(C)]
pub struct Preview {
    image_buf: *const u8,
    image_buf_size: usize,
    preview_buf: *const u8,
    preview_buf_size: usize,
    err: *mut c_char,
}

mod ffi {
    extern "C" {
        pub fn make_jpg_with_preview(
            size: u32,
            buf: *const u8,
            buf_size: usize,
        ) -> *const super::Preview;
    }
}

pub fn make_jpg_with_preview(size: u32, buf: &[u8]) -> Result<(Vec<u8>, Vec<u8>), Error> {
    ensure!(size != 0);
    ensure!(!buf.is_empty());
    unsafe {
        let preview = &*ffi::make_jpg_with_preview(size, buf.as_ptr(), buf.len());
        if !preview.err.is_null() {
            let err = CStr::from_ptr(preview.err).to_string_lossy();
            bail!("ffi exception: {:?}", err);
        }
        let image = slice::from_raw_parts(preview.image_buf, preview.image_buf_size as usize);
        let preview = slice::from_raw_parts(preview.preview_buf, preview.preview_buf_size as usize);
        Ok((image.to_vec(), preview.to_vec()))
    }
}

#[cfg(test)]
mod tests {
    use failure::Error;
    use image::{
        DynamicImage, ImageBuffer,
        ImageOutputFormat::{self, BMP, JPEG, PNG},
        Rgb,
    };
    use img_hash::HasherConfig;

    fn test_img(format: ImageOutputFormat, size: u32) -> Result<Vec<u8>, Error> {
        let mut buf = Vec::<u8>::new();
        DynamicImage::ImageRgb8(ImageBuffer::from_fn(size, size, |x, y| {
            Rgb([(x % (y + 1)) as u8, (x + y) as u8, (y % (x + 1)) as u8])
        }))
        .write_to(&mut buf, format)?;
        Ok(buf)
    }

    fn resize_and_compare(
        input_format: ImageOutputFormat,
        size: u32,
        size2: u32,
    ) -> Result<(), Error> {
        let test_img = test_img(input_format.clone(), size)?;
        let res = super::make_jpg_with_preview(size2, &test_img)?;
        let img = image::load_from_memory(&test_img)?;
        let img2 = image::load_from_memory(&res.1)?;
        let hasher = HasherConfig::new().to_hasher();
        let dist = hasher.hash_image(&img2).dist(&hasher.hash_image(&img));
        assert!(
            dist < 4,
            format!(
                "images are different; dist: {:} format: {:?}",
                dist, input_format
            )
        );
        Ok(())
    }
    #[test]
    fn empty_data() -> Result<(), Error> {
        assert!(super::make_jpg_with_preview(0, &[]).is_err());
        Ok(())
    }
    #[test]
    fn bad_data() -> Result<(), Error> {
        assert!(super::make_jpg_with_preview(100, &[1, 2, 3]).is_err());
        Ok(())
    }
    #[test]
    fn formats() -> Result<(), Error> {
        for input_format in vec![PNG, JPEG(90), BMP] {
            resize_and_compare(input_format, 200, 100)?;
        }
        Ok(())
    }

    #[test]
    fn upscale() -> Result<(), Error> {
        resize_and_compare(PNG, 200, 300)?;
        Ok(())
    }

    #[test]
    fn to_zero_size() -> Result<(), Error> {
        assert!(super::make_jpg_with_preview(0, &test_img(PNG, 200)?).is_err());
        Ok(())
    }

    #[test]
    fn to_one_px() -> Result<(), Error> {
        super::make_jpg_with_preview(1, &test_img(PNG, 200)?)?;
        Ok(())
    }

    #[test]
    fn from_one_px() -> Result<(), Error> {
        super::make_jpg_with_preview(100, &test_img(PNG, 1)?)?;
        Ok(())
    }
}
