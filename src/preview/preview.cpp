#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

struct Preview
{
  uint8_t *image_buf;
  size_t image_buf_size;
  uint8_t *preview_buf;
  size_t preview_buf_size;
  char *err;
};

Rect find_inscribed_square(Size size);

extern "C" Preview *make_jpg_with_preview(uint32_t preview_size, const uint8_t *const buf, size_t buf_size)
{
  Preview *res = new Preview();
  try
  {
    vector<uint8_t> bin(buf, buf + buf_size);
    vector<uint8_t> *jpg_bin = new vector<uint8_t>();
    Mat img = imdecode(bin, IMREAD_COLOR);
    imencode(".jpg", img, *jpg_bin);

    Mat preview, img_central_part = img(find_inscribed_square(img.size()));
    vector<uint8_t> *preview_bin = new vector<uint8_t>();
    resize(img_central_part, preview, Size(preview_size, preview_size), 0, 0, INTER_AREA);
    imencode(".jpg", preview, *preview_bin);

    res->image_buf = jpg_bin->data();
    res->image_buf_size = jpg_bin->size();
    res->preview_buf = preview_bin->data();
    res->preview_buf_size = preview_bin->size();
  }
  catch (const exception &ex)
  {
    char const *err = ex.what();
    res->err = new char[strlen(err) + 1];
    strcpy(res->err, err);
  }
  return res;
}

Rect find_inscribed_square(Size size)
{
  auto width = size.width;
  auto height = size.height;
  Rect rect(0, 0, width, height);
  Point center = (rect.br() + rect.tl()) / 2;
  auto radius = width > height ? height : width;
  auto x = center.x - radius / 2;
  auto y = center.y - radius / 2;
  return Rect(x, y, radius, radius);
}
