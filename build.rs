use std::{env, process::Command, str};

fn main() {
    if cfg!(target_os = "macos") {
        let res = Command::new("brew")
            .arg("--prefix")
            .arg("opencv")
            .output()
            .expect("brew");
        let opencv_path = str::from_utf8(&res.stdout).unwrap();
        let opencv_path = [opencv_path.trim_end(), "/lib/pkgconfig/"].concat();
        let key = "PKG_CONFIG_PATH";
        if env::var(key).is_err() {
            env::set_var(key, opencv_path);
        }
    }
    let opencv = pkg_config::probe_library("opencv4").unwrap();
    let mut builder = cc::Build::new();
    builder
        .file("src/preview/preview.cpp")
        .flag("-std=c++11")
        .cpp(true);
    for p in &opencv.include_paths {
        builder.include(p);
    }
    builder.compile("preview.a");
}
