FROM debian:buster-slim as builder

ARG DEBUG
ENV RUST_VERSION=1.39.0
ENV OPENCV_VERSION=4.1.2
ENV PKG_CONFIG_ALL_STATIC=1

RUN apt-get update && apt-get install -y curl unzip make g++ pkg-config cmake libssl-dev zlib1g-dev zip

# install rust
ENV PATH=/root/.cargo/bin:$PATH
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal \
    && rustup install nightly \
    && rustup install $RUST_VERSION \
    && rustup default $RUST_VERSION \
    && rustup component add clippy \
    && curl -L https://github.com/mozilla/grcov/releases/download/v0.5.5/grcov-linux-x86_64.tar.bz2 | tar jxf - -C /bin

# build opencv
RUN curl -OL https://github.com/opencv/opencv/archive/$OPENCV_VERSION.zip \
    && unzip -q $OPENCV_VERSION.zip \
    && rm $OPENCV_VERSION.zip \
    && mv opencv-$OPENCV_VERSION /tmp/opencv \
    && mkdir -p /tmp/opencv/build \
    && cd /tmp/opencv/build \
    && cmake \
    -D CMAKE_BUILD_TYPE=$([ $DEBUG ] && echo 'DEBUG' || echo 'RELEASE') \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D BUILD_JPEG=ON \
    -D BUILD_PNG=ON \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D BUILD_SHARED_LIBS=OFF \
    -D BUILD_DOCS=OFF \
    -D BUILD_EXAMPLES=OFF \
    -D BUILD_opencv_apps=OFF \
    -D BUILD_PERF_TESTS=OFF \
    -D WITH_FFMPEG=OFF \
    -D WITH_TIFF=OFF \
    -D WITH_WEBP=OFF \
    -D WITH_JASPER=OFF \
    -D WITH_OPENEXR=OFF \
    -D WITH_TBB=OFF \
    -D WITH_IPP=OFF \
    -D WITH_ITT=OFF \
    -D WITH_PROTOBUF=OFF \
    -D WITH_OPENCL=OFF \
    -D WITH_V4L=OFF \
    -D WITH_IMGCODEC_HDR=OFF \
    -D WITH_IMGCODEC_SUNRASTER=OFF \
    -D WITH_IMGCODEC_PXM=OFF \
    -D WITH_IMGCODEC_PFM=OFF \
    -D WITH_QUIRC=OFF \
    -D BUILD_TESTS=OFF .. \
    && make -j`nproc` \
    && make install \
    # remove build dir and fix opencv4.pc bug
    && mv /tmp/opencv/build/lib /tmp \
    && rm -rf /tmp/opencv \ 
    && mkdir -p  /tmp/opencv/build \
    && mv /tmp/lib /tmp/opencv/build 

ENV COV_RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Cinline-threshold=0 -Clink-dead-code -Coverflow-checks=off -Zno-landing-pads"

# RUN cargo install cargo-tarpaulin

# build deps
RUN mkdir /tmp/app 
WORKDIR /tmp/app
RUN USER=$(whoami) cargo init 
COPY build.rs Cargo.toml Cargo.lock /tmp/app/
COPY src/preview/preview.cpp /tmp/app/src/preview/preview.cpp
RUN cargo clippy $([ $DEBUG ] || echo '--release') 
RUN CARGO_INCREMENTAL=0 \
    RUSTFLAGS=$COV_RUSTFLAGS \ 
    cargo +nightly test --no-run
# RUN cargo tarpaulin -v --no-run

# build app
COPY src /tmp/app/src
COPY conf /tmp/app/conf
COPY tests /tmp/app/tests
RUN cargo clippy $([ $DEBUG ] || echo '--release') 
RUN cargo build $([ $DEBUG ] || echo '--release') 
# RUN cargo tarpaulin -v --no-run

RUN CARGO_INCREMENTAL=0 RUSTFLAGS=$COV_RUSTFLAGS cargo +nightly test
RUN zip -q0 ccov.zip $(find target -name "*preview_api*.gc*" -print) \
    && grcov ccov.zip -s src -t html --ignore "/*" \
    && rm ccov.zip \
    && echo line_coverage: $(grep -oP 'linesPercentage[^\d]+\d+\.\d+\s*%' html/index.html | grep -oP '\d+\.\d+\s*%')
RUN cp target/$([ $DEBUG ] && echo 'debug' || echo 'release')/preview_api /bin 

CMD cp -r /tmp/app/html /
# CMD cargo tarpaulin -v -o Html && cp tarpaulin-report.html /html

# run in fresh image
FROM debian:buster-slim
RUN apt-get update && apt-get install -y libssl-dev
COPY --from=builder /tmp/app/conf /tmp/app/conf
COPY --from=builder /bin/preview_api /bin
WORKDIR /tmp/app

CMD preview_api