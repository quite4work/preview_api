use actix_rt::{System, SystemRunner};
use actix_web::{App, HttpServer};
use awc::Client;
use failure::{bail, format_err, Error};
use futures::{future::lazy, Future};
use image::{
    DynamicImage, ImageBuffer,
    ImageOutputFormat::{self, BMP, JPEG, PNG},
    Rgb,
};
use preview_api::{
    configure_service,
    settings::CONFIG,
    upload::{Req, ReqImage, Resp},
};
use reqwest::multipart;
use std::{env, fs, str::FromStr, sync::Once};
use url::Url;

static INIT: Once = Once::new();
use once_cell::sync::Lazy;

static SERVER_URL: Lazy<String> =
    Lazy::new(|| env::var("SERVER_URL").unwrap_or_else(|_| "http://127.0.0.1:8080".into()));

#[test]
fn base64_json() -> Result<(), Error> {
    let images = vec![BMP, JPEG(90), PNG]
        .into_iter()
        .map(|format| Ok(ReqImage::Base64(test_img(format, 200)?)))
        .collect::<Result<Vec<ReqImage>, Error>>()?;
    let resp = upload_json(images)?;
    if let Resp::Ok { images } = serde_json::from_str(&resp)? {
        assert_eq!(images.len(), 3);
    } else {
        bail!(resp);
    };
    Ok(())
}

#[test]
fn url_json() -> Result<(), Error> {
    let images = vec![ReqImage::Url(
        Url::from_str("https://www.rustacean.net/assets/cuddlyferris.png").unwrap(),
    )];
    let resp = upload_json(images)?;
    if let Resp::Ok { images } = serde_json::from_str(&resp)? {
        assert_eq!(images.len(), 1);
    } else {
        bail!(resp);
    };
    Ok(())
}

#[test]
fn real_images() -> Result<(), Error> {
    let images = vec!["balloons.jpg", "firefox.png"]
        .into_iter()
        .map(|file| Ok(ReqImage::Base64(fs::read(format!("tests/data/{}", file))?)))
        .collect::<Result<Vec<ReqImage>, Error>>()?;
    let resp = upload_json(images)?;
    if let Resp::Ok { images } = serde_json::from_str(&resp)? {
        assert_eq!(images.len(), 2);
    } else {
        bail!(resp);
    };
    Ok(())
}

#[test]
fn upload_multipart() -> Result<(), Error> {
    let _system = setup();
    let form = multipart::Form::new()
        .file("f", "tests/data/firefox.png")?
        .file("f2", "tests/data/balloons.jpg")?;
    if let Resp::Ok { images } = reqwest::Client::new()
        .post(&[&SERVER_URL, "/upload"].concat())
        .multipart(form)
        .send()?
        .json()?
    {
        assert_eq!(images.len(), 2);
    };
    Ok(())
}

fn upload_json(images: Vec<ReqImage>) -> Result<String, Error> {
    let mut system = setup();
    let body: bytes::Bytes = system.block_on(lazy(|| {
        Client::default()
            .post([&SERVER_URL, "/upload"].concat())
            .send_json(&Req { images })
            .map_err(|e| format_err!("1 {}", e))
            .and_then(|mut resp| resp.body().map_err(|e| format_err!("2 {}", e)))
    }))?;
    Ok(String::from_utf8(body.to_vec())?)
}

fn test_img(format: ImageOutputFormat, size: u32) -> Result<Vec<u8>, Error> {
    let mut buf = Vec::<u8>::new();
    DynamicImage::ImageRgb8(ImageBuffer::from_fn(size, size, |x, y| {
        Rgb([(x % (y + 1)) as u8, (x + y) as u8, (y % (x + 1)) as u8])
    }))
    .write_to(&mut buf, format)?;
    Ok(buf)
}

pub fn setup() -> SystemRunner {
    let system = System::new("test");
    INIT.call_once(|| {
        if env::var("SERVER_URL").is_err() {
            env_logger::init();
            fs::create_dir_all(&CONFIG.upload.storage.path).unwrap();
            HttpServer::new(|| App::new().configure(configure_service))
                .bind(&CONFIG.webservice.listen)
                .unwrap()
                .start();
        }
    });
    system
}
