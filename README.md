# preview_api

[![pipeline status](https://gitlab.com/quite4work/preview_api/badges/master/pipeline.svg)](https://gitlab.com/quite4work/preview_api/commits/master)
[![coverage report](https://gitlab.com/quite4work/preview_api/badges/master/coverage.svg)](https://quite4work.gitlab.io/preview_api/)

Image upload api with preview generation in Rust with OpenCV and ActixWeb

Main features:

- Upload multiple files
- Upload picture from url
- Upload Base64 encoded pictures in json request
- Support of `multipart/form-data`
- Generation of 100x100 px preview

Additional features:

- OpenCV with static linking support
- [Release image is built and tested in GitLab CI with build cache configured](https://gitlab.com/quite4work/preview_api/pipelines)
- Unit and integration tests
- Graceful shutdown
- Docker Compose
- Parallel processing of pictures
- Configuration file
- [Code Coverage](https://quite4work.gitlab.io/preview_api/)
- [Pushing docker images to CI registry after success CI build](https://gitlab.com/quite4work/preview_api/container_registry)
- Multistage container building

## Building and Running

### Docker

Running latest prebuilt image:

```sh
docker pull registry.gitlab.com/quite4work/preview_api
docker-compose up
```

Building and running:

```sh
docker-compose build
docker-compose up
```

### MacOS

Building and running:

```sh
brew install opencv
cargo run
```

Clippy:

```sh
rustup component add clippy
cargo clippy
```

Tests:

```sh
cargo test
```

## API

### Request

`POST /upload`

Content-Type:

- `multipart/form-data` \*see examples
- `application/json`

```json
{
  "images": ["[url]", "[base64_encoded_image]", ...]
}
```

### Response

```json
{
  "images": [{ "image": "[url]", "preview": "[url]"}, ...]
}
```

### Error

```json
{
  "error_description": "[human_redable_description]"
}
```

### Examples

- Json

```sh
curl -X POST http://127.0.0.1:8080/upload -H 'Content-Type: application/json' -d '{"images":["'"$(base64 -w0 tests/data/firefox.png)"'","https://www.rustacean.net/assets/cuddlyferris.png"]}'
```

- Form-Data

```sh
curl -X POST http://127.0.0.1:8080/upload -H 'Content-Type: multipart/form-data' -F k=@tests/data/balloons.jpg -F k=@tests/data/firefox.png
```

- Response

```json
{
  "images": [
    {
      "image": "http://127.0.0.1:8080/storage/f939be9c2d90b3b2.jpg",
      "preview": "http://127.0.0.1:8080/storage/f939be9c2d90b3b2_preview.jpg"
    },
    {
      "image": "http://127.0.0.1:8080/storage/d993f123e354ca0f.jpg",
      "preview": "http://127.0.0.1:8080/storage/d993f123e354ca0f_preview.jpg"
    }
  ]
}
```
